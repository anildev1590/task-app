<table class="table table-hover text-nowrap">
    <thead>
        <tr>
            <th>#Id</th>
            <th>Title</th>
            <th>Description</th>
            <th>No of Images Required</th>
            <th>Picked By</th>
            <th>Images Reference</th>
            <th>Completed</th>
            <th>Created At</th>
        </tr>
    </thead>

    <tbody>
        @if(!empty($tasks) && $tasks->count())
            @foreach($tasks as $key => $task)
                <tr>
                    <td>{{ $task->id }}</td>
                    <td>{{ $task->title }}</td>
                    <td>{{ $task->description }}</td>
                    <td>{{ $task->no_of_images }}</td>
                    <td>{{ $task->picked_by ?? 'Not Picked Yet' }}</td>
                    <td>{{ $task->images_reference }}</td>
                    <td>{{ $task->completed }}</td>
                    <td>{{ $task->created_at->format('Y-m-d') }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" class="text-center">There are no data.</td>
            </tr>
        @endif
    </tbody>
</table>

{!! $tasks->links() !!}