<div class="py-12">
    <div class="mx-auto sm:px-6">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            @include('livewire.partials.messages')
            
            <div class="p-6 bg-white border-b border-gray-200" x-data="{count : 0}">
                <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between mb-4">
                    <div class="section-heading">
                        <div class="text-xl font-bold">{{ $task->title }}</div>
                        <p>No of required images: {{ $task->no_of_images }}</p>
                        <div>Selected Images: <span x-text="count"></span></div>
                    </div>
                </div>

                <div class="mt-6 text-gray-500">
                    {{ $task->description }}
                </div>

                <div class="mt-6 text-gray-500">
                    <input class="border-solid border sm:text-sm sm:leading-5 rounded border-gray-300 w-full md:w-1/4" type="text" placeholder="({{ $active_service }}) Search for images here..." wire:model="image_search"/>
                </div>

                <div class="mt-6 text-gray-500">
                    <form>
                        <div class="grid grid-cols-4 gap-2 mx-auto">
                            @if(!empty($images) && count($images))
                                @foreach($images as $key => $image)
                                    <div class="w-full rounded" wire:key="{{ $key }}">
                                        <label for="checkbox-{{ $key }}">
                                            <img src="{{ $image }}" alt="loading..." class="object-cover rounded rounded-b-none w-full" style="height: 12.5rem;">

                                            <div class="bg-gray-100 border h-10 p-2 rounded w-full rounded-t-none border-gray-300">
                                                <input @click="count += ($event.target.checked) ? + 1 : -1" type="checkbox" value="{{ $image }}" wire:model.defer="images_reference.{{$key}}" class="form-checkbox rounded h-5 w-5"id="checkbox-{{ $key }}"> Select
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            @else
                                <h4>Your search will appear here</h4>
                            @endif
                        </div>

                        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between mt-4">
                            <div>
                                @error('images_reference') <span class="text-red-500 bg-red-100">{{ $message }}</span>@enderror
                            </div>
                            <button wire:click.prevent="store()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Mark Completed {{ $images_reference ? '(' . count(array_filter($images_reference)) . ')' : ''}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>