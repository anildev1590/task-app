<x-slot name="header">
    <h2 class="font-semibold text-gray-800 leading-tight"> Tasks (Assign Task To Yourself)</h2>
</x-slot>

<div class="py-12">
    <div class="mx-auto sm:px-6">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-2 py-4">
            @include('livewire.partials.messages')

            <div class="sm:flex-1 sm:flex sm:flex-row-reverse mb-4">
                <input class="border-solid border sm:text-sm sm:leading-5 rounded border-gray-300 w-full md:w-1/4" type="text" placeholder="Search by title, description & users name" wire:model="filter"/>
            </div>

            <table class="w-full sm:text-sm">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-2 py-2 w-20">#Id</th>
                        <th class="px-2 py-2">Title</th>
                        <th class="px-2 py-2">Description</th>
                        <th class="px-2 py-2">No Of Images</th>
                        <th class="px-2 py-2">Created At</th>
                        <th class="px-2 py-2">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @if(!empty($tasks) && $tasks->count())
                        @foreach($tasks as $task)
                            <tr>
                                <td class="border px-2 py-2">{{ $task->id }}</td>
                                <td class="border px-2 py-2">{{ $task->title }}</td>
                                <td class="border px-2 py-2">{{ $task->description }}</td>
                                <td class="border px-2 py-2">{{ $task->no_of_images }}</td>
                                <td class="border px-2 py-2">{{ $task->created_at->format('Y-m-d') }}</td>
                                <td class="border px-2 py-2">
                                    <button wire:click="take({{ $task->id }})" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-1 rounded">Take Task</button>
                                </td>
                            </tr>
                        @endforeach
                     @else
                        <tr>
                            <td colspan="9" class="border px-2 py-2 text-center">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <div class="pt-4">
                {{ $tasks->links() }}
            </div>
        </div>
    </div>
</div>