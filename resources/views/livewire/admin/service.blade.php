<div class="form-row" style="
    position: absolute;
    top: 75px;
    right: 25px;"
>
    @foreach($services as $service)
        <div class="form-group col-md-1 pl-4">
            <input type="radio" wire:click="toggleService('{{ $service->id }}', '{{ $service->service }}')" name="service" id="service-{{ $service->id }}" class="form-check-input" {{ $service->active ? 'checked' : '' }}>
            <label for="service-{{ $service->id }}">Search Image Api Selection - {{ $service->service }}</label>
        </div>
    @endforeach
</div>