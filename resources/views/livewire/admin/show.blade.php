<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
	<div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
		<div class="fixed inset-0 transition-opacity">
			<div class="absolute inset-0 bg-gray-500 opacity-75"></div>
		</div>
		
		<!-- This element is to trick the browser into centering the modal contents. -->
		<span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
		<div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-2xl sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
			<table class="w-full sm:text-sm">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-2 py-2">Preview</th>
                        <th class="px-2 py-2">Image URL</th>
                    </tr>
                </thead>

                <tbody>
                    @if(!empty($images_reference) && count($images_reference))
                        @foreach($images_reference as $key => $image)
                            <tr>
                                <td class="border px-2 py-2">
                                    <div class="bg-sky-300 ...">
                                      <img src="{{ $image }}" alt="loading..." class="object-cover h-10 w-60">
                                    </div>
                                </td>
                                <td class="border px-2 py-2">
                                    <a href="{{ $image }}" target="_blank">{{ $image }}</a>
                                </td>
                            </tr>
                        @endforeach
                     @else
                        <tr>
                            <td colspan="9" class="border px-2 py-2 text-center">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
					<span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
						<button wire:click="closeModal()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"> Close </button>
					</span>
				</div>
		</div>
	</div>
</div>