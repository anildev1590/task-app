<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Service;

class ServiceComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('services', Service::all());
    }
}
