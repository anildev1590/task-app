<?php
 
namespace App\Http\Livewire\Staff;
 
use Livewire\Component;
use Livewire\WithPagination;
use App\Services\IntegrationService;
use App\Models\Service;
use App\Models\Task;
use Auth;

class Tasks extends Component
{
    use WithPagination;

    public $filter, $image_search, $task_id, $no_of_images, $images_reference = [], $active_service;
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $service = Service::active()->first();
        $this->active_service = $service->service;

        if($ifAlreadyPicked = Task::pickedOngoingTask()->first()) {
            $this->task_id = $ifAlreadyPicked->id;
            $this->no_of_images = $ifAlreadyPicked->no_of_images;
            $images = $this->image_search ? $this->_getImages() : [];

            return view('livewire.staff.ongoing-task', [
                'task' => $ifAlreadyPicked,
                'images' => $images
            ]);
        }

        return view('livewire.staff.tasks', [
            'tasks' => Task::onlyUnassigned()->filters($this->filter)->paginate(15),
        ]);
    }
      
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function take($id)
    {
        Task::findOrFail($id)->update(['picked_by' => Auth::user()->id]);
        session()->flash('message', 'Task Assigned Successfully.');
        return redirect()->route('staff.task');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->images_reference = array_values(
            array_filter($this->images_reference)
        );

        $this->validate([
            'images_reference' => "required|array|min:{$this->no_of_images}|max:{$this->no_of_images}",
        ]);
    
        Task::findOrFail($this->task_id)->update([
            'images_reference' => $this->images_reference,
            'completed' => 1,
        ]);

        session()->flash('message', 'Task Completed Successfully.');
    }

    /**
     * function to get/search images from active service
     *
     * @var array
     */
    private function _getImages() {
        $imagesArr = [];
        $response = IntegrationService::{$this->active_service}('GET', 'images/search', [
                'query' => $this->image_search,
                'per_page' => 20,
                'image_type' => 'photo'
        ]);

        if($response['code'] == 200) {
            if($this->active_service == Service::SHUTTERSTOCK) {
                foreach($response['response']['data'] as $images) {
                    $imagesArr[] = $images['assets']['preview']['url'];
                }
            } elseif($this->active_service == Service::STORYBLOCKS) {
                foreach($response['response']['results'] as $images) {
                    $imagesArr[] = $images['thumbnail_url'];
                }
            }

            return $imagesArr;
        }
    }

}