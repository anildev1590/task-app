<?php
 
namespace App\Http\Livewire\Admin;
 
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Service;
use App\Models\Task;

class Tasks extends Component
{
    use WithPagination;

    public $task_id, $title, $description, $no_of_images, $images_reference, $filter;
    public $isOpen = 0;
    public $detailsModal = 0;
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        return view('livewire.admin.tasks', [
            'tasks' => Task::filters($this->filter)->paginate(15)
        ]);
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
        $this->detailsModal = false;
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields()
    {
        $this->task_id = '';
        $this->title = '';
        $this->description = '';
        $this->no_of_images = '';
    }
      
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
            'no_of_images' => 'required',
        ]);
    
        Task::updateOrCreate(['id' => $this->task_id], [
            'title' => $this->title,
            'description' => $this->description,
            'no_of_images' => $this->no_of_images,
        ]);
   
        session()->flash('message', 
            $this->task_id ? 'Task Updated Successfully.' : 'Task Created Successfully.');
   
        $this->closeModal();
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $Task = Task::findOrFail($id);
        $this->task_id = $Task->id;
        $this->title = $Task->title;
        $this->description = $Task->description;
        $this->no_of_images = $Task->no_of_images;
     
        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function show($id)
    {
        $Task = Task::findOrFail($id);
        $this->images_reference = $Task->images_reference;
        
        $this->detailsModal = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function toggleService($serviceId, $service)
    {
        Service::query()->update(['active' => 0]);
        Service::findOrFail($serviceId)->update(['active' => 1]);
        session()->flash('message', "{$service} Service Activated Successfully");
    }
      
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Task::find($id)->delete();
        session()->flash('message', 'Task Deleted Successfully.');
    }
}