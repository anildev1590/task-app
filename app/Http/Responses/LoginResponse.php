<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
use App\Providers\RouteServiceProvider;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {
        $redirectTo = Auth::user()->role_id == 1
                    ? config('fortify.admin')
                    : config('fortify.staff');

        return $request->wantsJson()
                    ? response()->json(['two_factor' => false])
                    : redirect()->intended($redirectTo);
    }

}