<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\Task;
use Auth;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $tasks = Task::filters($request->all())->paginate(20);

        if ($request->has('search')) {
            $html = View::make('admin.table', ['tasks' => $tasks]);
            return response()->json([
                'status' => 'success',
                'html' => $html->render()
            ], 200);
        }

        return view('admin.task.index', compact('tasks'));
    }
}
