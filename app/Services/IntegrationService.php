<?php

namespace App\Services;

use Session;
use Illuminate\Support\Facades\Http;

class IntegrationService
{
    public static function ShutterStock($method, $endPoint, $params = null)
    {
        $url = config('app.shutterstock_base_url') . config('app.shutterstock_api_version') .'/'. $endPoint;
        $request = Http::withBasicAuth(config('app.shutterstock_username'), config('app.shutterstock_password'))->acceptJson();
        return self::integrationHttpRequest($method, $url, $request, $params);
    }

    public static function StoryBlocks($method, $endPoint, $params = null)
    {
        $expires = time() + 100;
        $resource = config('app.storyblocks_api_version') .'/'. $endPoint;

        $additionalParams = [
            'EXPIRES' => $expires,
            'APIKEY' => config('app.storyblocks_public_key'),
            'project_id' => config('app.storyblocks_project_id'),
            'user_id' => '<string>',
            'keywords' => isset($params['query']) ? $params['query'] : "",
            'results_per_page' => isset($params['per_page']) ? $params['per_page'] : "20",
            'HMAC' => hash_hmac("sha256", "/" . $resource, config('app.storyblocks_private_key') . $expires)
        ];

        $url = config('app.storyblocks_base_url') . config('app.storyblocks_api_version') .'/'. $endPoint;
        $request = Http::acceptJson();
        return self::integrationHttpRequest($method, $url, $request, $additionalParams);
    }

    public static function integrationHttpRequest($method, $url, $request, $params = null)
    {
        try {
            $response = $request->get($url, $params);

            $result = $response->json();
            if($response->successful()) {
                return ['response' => $result, 'code' => $response->status()];     
            } else {
                \Log::error($response->getReasonPhrase() . ' on '. $customMessage. ' having Errors Response: '.json_encode($result,true));
                return ['response' => null, 'code' => $response->status()];
            } 
        } catch (\Exception $exception) {
            \Log::error('GeneralException ' . $exception->getMessage()  . ' on ' . $exception->getTraceAsString());
            return ['response' => null, 'code' => $exception->getCode()];
        }
    }
}