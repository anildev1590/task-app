<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['service', 'active'];

    const SHUTTERSTOCK = 'ShutterStock';
    const STORYBLOCKS = 'StoryBlocks';

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }
}
