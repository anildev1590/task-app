<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Task extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $casts = ['images_reference' => 'array'];
    protected $fillable = ['title', 'description', 'no_of_images', 'picked_by', 'completed', 'images_reference'];

    /**
     * Get the decoded add active addons data.
     *
     * @param string $value
     * @return array
     */
    public function getImagesReferenceAttribute($value) {
        return (array) json_decode($value);
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'picked_by', 'id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyAssigned($query) {
        return $query->whereNotNull('picked_by');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyUnassigned($query) {
        return $query->whereNull('picked_by');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePickedOngoingTask($query) {
        return $query->where('picked_by', Auth::user()->id)
                     ->where('completed', 0);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $filter) {
        if (isset($filter) && !empty($filter)) {
            $query->where('title', 'LIKE', "%{$filter}%")
                ->orWhere('description', 'LIKE', "%{$filter}%")
                ->orWhereHas('user', function($userQuery) use ($filter) {
                    return $userQuery->where('name', 'LIKE', "%{$filter}%");
                });
        }
        return $query;
    }
}
