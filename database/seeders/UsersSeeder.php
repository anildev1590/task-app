<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        $usersArr = [
            ["role_id" => 1, "name" => "Admin", "email" => "admin@todo.com", "password" => bcrypt("12345678")],
            ["role_id" => 2, "name" => "Staff User", "email" => "staffuser@todo.com", "password" => bcrypt("12345678")],
            ["role_id" => 2, "name" => "Staff User 2", "email" => "staffuser2@todo.com", "password" => bcrypt("12345678")],
        ];
        DB::table('users')->insert($usersArr);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
