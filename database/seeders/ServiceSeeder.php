<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('services')->delete();
        $servicesArr = [
            ['service' => 'ShutterStock', 'active' => 1],
            ['service' => 'StoryBlocks', 'active' => 0],
        ];
        DB::table('services')->insert($servicesArr);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
