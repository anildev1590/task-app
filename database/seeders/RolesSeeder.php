<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->delete();
        $rolesArr = [
            ['id' => 1, 'role' => 'Admin', 'description' => 'Admin'],
            ['id' => 2, 'role' => 'Staff', 'description' => 'Staff'],
        ];
        DB::table('roles')->insert($rolesArr);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
