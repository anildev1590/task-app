<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  // Provided by Storyblocks
  $publicKey = "test_8e83abb7936a1b904184fb6f8888c2470c2488f498a26f5b382c3f19537";
  $privateKey = "test_44f23b7c302eb15909f005159dd9afb9c843cf1fe6545cfae32a060693e";
 
  // url info
  $baseUrl = "https://api.graphicstock.com";
  $resource = "/api/v2/images/search";

  // HMAC generation
  $expires = time() + 100; // Note, this returns current Unix timestamp in seconds
  $hmac = hash_hmac("sha256", $resource, $privateKey . $expires);
 
  $curl = curl_init();
  $url = "https://api.graphicstock.com/api/v2/images/search?EXPIRES=$expires&APIKEY=$publicKey&project_id=asjlkfks2&user_id=<string>&keywords=school&HMAC=$hmac";
  // echo $url; 
  curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => '',
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => 'GET',
  ));
  if (curl_errno($curl)) {
	  $error_msg = curl_error($curl);
  }
 
  
  if (isset($error_msg)) {
	  echo "curl error ".$error_msg;
  }
  $response = curl_exec($curl);
  
  curl_close($curl);
  var_dump($response);
  echo $response;
?>